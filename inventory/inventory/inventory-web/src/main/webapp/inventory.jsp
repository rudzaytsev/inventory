<%-- 
    Document   : inventory
    Created on : 09.12.2014, 22:17:34
    Author     : rudolph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inventory</title>
    </head>
    <body>
        <h1>Inventory of Mobile Devices</h1>
        
        <form action="InventoryServlet" method="post">
            <input type="hidden" name="inventoryupdate" value="UPDATE"/>
            <input type="submit" value="UPDATE">
        </form>
        <table>
            <thead align="center">    
                <tr>
                    <td>#</td>                                        
                    <td> OS name </td>
                    <td> OS version </td>
                    <td> Kernel version </td>
                    <td> CPU </td>
                    <td> Memory (MB) </td>
                    <td> Watch Full Information </td>                    
                </tr>
            </thead>
            <tbody align="center">
                <c:forEach var="device" items="${devices}">
                 <tr>
                    <td>${device.getId()}               </td>                                     
                    <td>${device.hardware.osname}       </td>
                    <td>${device.hardware.osversion}    </td>
                    <td>${device.hardware.oscomments}   </td>
                    <td>${device.hardware.processorT}       </td>
                    <td>${device.hardware.memory}      </td>
                    <td> 
                        <form action="InventoryServlet" method="post">
                            <input type="hidden" name="id" value="${device.getId()}"/>
                            <input type="submit" value="watch">
                        </form>
                    </td>
                    
                 </tr>
                </c:forEach>
            </tbody>    
        </table>    
    </body>
</html>
