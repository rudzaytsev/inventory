<%-- 
    Document   : device_info
    Created on : 14.12.2014, 0:44:22
    Author     : rudolph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Device info</title>
    </head>
    <body>
        <h1>Device Information</h1>
        <table>
            <thead>
                <tr>
                    <td> <h2>Bios</h2>     </td>
                    <td> <h2>Hardware</h2> </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td>    
                <div id="divBios">

                    <table>
                        <tr>
                            <td> assettag: <td>
                            <td>${wdevice.bios.assettag} </td>
                        </tr>
                        <tr>
                            <td> smodel: <td>
                            <td>${wdevice.bios.smodel} </td>    
                        </tr>        
                        <tr>
                            <td> ssn: <td>    
                            <td>${wdevice.bios.ssn} </td>
                        </tr>        
                        <tr>
                            <td> bmanufacturer: <td>    
                            <td>${wdevice.bios.bmanufacturer} </td>
                        </tr>        
                        <tr>
                            <td> smanufacturer: <td>    
                            <td>${wdevice.bios.smanufacturer} </td>
                        </tr>        
                        <tr>
                            <td> bdate: <td>    
                            <td>${wdevice.bios.bdate} </td>
                        </tr>        
                        <tr>
                            <td> mmanufacturer: <td>      
                            <td>${wdevice.bios.mmanufacturer} </td>
                        </tr>        
                        <tr>
                            <td> bversion: <td>    
                            <td>${wdevice.bios.bversion} </td>
                        </tr>        
                        <tr>
                            <td> type: <td>     
                            <td>${wdevice.bios.type} </td>
                        </tr>
                    </table>
                </div>
                </td>
                <td>
                <div id="divHardware">    

                    <table>
                        <tr>
                            <td> processorT:  </td>
                            <td>${wdevice.hardware.processorT}   </td>
                        </tr>
                        <tr>
                            <td> processorS:  </td>
                            <td> ${wdevice.hardware.processorS}  </td>
                        </tr>
                        <tr>
                            <td> processorN:  </td>
                            <td> ${wdevice.hardware.processorN}  </td>
                        </tr>            
                        <tr>
                            <td> name:  </td>
                            <td> ${wdevice.hardware.name}  </td>
                        </tr>
                        <tr>
                            <td> osname:  </td>
                            <td> ${wdevice.hardware.osname}  </td>
                        </tr>
                        <tr>
                            <td> osversion:  </td>
                            <td> ${wdevice.hardware.osversion}  </td>
                        </tr>
                        <tr>
                            <td> oscomments:  </td>
                            <td> ${wdevice.hardware.oscomments}  </td>
                        </tr>
                        <tr>
                            <td> swap:  </td>
                            <td> ${wdevice.hardware.swap}  </td>
                        </tr>
                        <tr>
                            <td> workgroup:  </td>
                            <td> ${wdevice.hardware.workgroup}  </td>
                        </tr>
                        <tr>
                            <td> ipAddr:   </td>
                            <td> ${wdevice.hardware.ipAddr}  </td>
                        </tr>
                        <tr>
                            <td> lastLoggedUser:  </td>
                            <td> ${wdevice.hardware.lastLoggedUser}  </td>
                        </tr>
                        <tr>
                            <td> dateLastLoggedUser:  </td>
                            <td> ${wdevice.hardware.dateLastLoggedUser}  </td>
                        </tr>            
                        <tr>
                            <td> memory:  </td>
                            <td> ${wdevice.hardware.memory}  </td>
                        </tr>                      
                    </table>
                </div>
                </td>
                </tr>
            </tbody>    
        </table>
         
        <h2>Software</h2>                
        <table>
            <c:forEach var="soft" items="${wdevice.getSoftwares()}">
            <tr>
                <td colspan="2"> <h3> ${soft.name} </h3> </td>                
            </tr>
            <tr>
                <td> folder: </td>
                <td> ${soft.folder} </td>
            </tr>
            <tr>
                <td> version: </td>
                <td> ${soft.version}</td>
            </tr>
            <tr>
                <td> publisher: </td>
                <td> ${soft.publisher}</td>
            </tr>
            <tr>
                <td> installDate: </td>
                <td> ${soft.installDate}</td>
            </tr>
            <tr>               
                <td> comments: </td>
                <td> ${soft.comments}</td>
            </tr>
            <tr>               
                <td> fileSize </td>
                <td> ${soft.fileSize}</td>
            </tr>
            </c:forEach>
        </table>
           
    </body>
</html>
