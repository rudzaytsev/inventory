/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.utils;

import com.mycompany.domain.Bios;
import com.mycompany.domain.Device;
import com.mycompany.domain.DomainObject;
import com.mycompany.domain.Hardware;
import com.mycompany.domain.Software;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author rudolph
 */
public class XmlParser {
    
    public XmlParser(){        
    }
    
    public static XMLStreamReader createParser(String document){
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader parser;
        try {
            parser = factory.createXMLStreamReader(
                                            new ByteArrayInputStream(document.getBytes()), "UTF-8");
        } catch (XMLStreamException ex) {
            Logger.getLogger(XmlParser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return parser;
    }
    
   
    /**
     * 
     * @param input input XML document as string
     * @return content of tagName element
     *         or null if XML document has no tag with tagName
     */
    public String parseTagContent(String input, String tagName){
       
        XMLStreamReader parser = XmlParser.createParser(input);
        
        ParserStrategy parserStrategy;
        parserStrategy = new ParserStrategy(){
            
            @Override
            public void initParams() {
                //does nothing
            }
            
            @Override
            public Map<String, Object> parsingAlgorithm(XMLStreamReader parser, Map<String, Object> resultMap)
                    throws XMLStreamException {
                
                int event = parser.next();
                if(event == XMLStreamConstants.START_ELEMENT){
                    String elementName = parser.getLocalName();
                    if(elementName.equals(firstRequiredTagName)){
                        firstRequiredTagArrived = true;
                    }
                }
                else if(event == XMLStreamConstants.CHARACTERS){
                    if(firstRequiredTagArrived){
                        resultMap.put(resultKey,parser.getText());
                    } 
                }
                else if(event == XMLStreamConstants.END_ELEMENT){
                    if(firstRequiredTagArrived){
                        firstRequiredTagArrived = false;
                    } 
                }
                return resultMap;
                
            }

            
   
        };
        parserStrategy.setRequiredTagName(tagName);
        parserStrategy.setResultKey(tagName);
        Map<String,Object> parsingResMap = parserStrategy.parseContent(parser);
        
        return (String) parsingResMap.get(parserStrategy.getResultKey());
        
    }
    
    
    
    
    public Device parseDocument(String document) throws Exception{
        String result = this.parseTagContent(document,"QUERY");
        if(result == null){
            throw new Exception("Uncorrect XML Document. QUERY is not found");
        }
        if(result.equals("INVENTORY")){
           return  parseDeviceInventoryInfo(document);
        }
        return null;
    }
    
    
    public Device  parseDeviceInventoryInfo(String document){
        
        Device device = new Device(parseDeviceId(document));
        device.setBios(parseBios(document));
        device.setHardware(parseHardware(document));
        device.setSoftwares(parseSoftwares(document));
        return device;
        
    }
    
    public String parseDeviceId(String document){
        return parseTagContent(document,"DEVICEID");
    }
    
    public Bios parseBios(String document){
        XMLStreamReader parser = XmlParser.createParser(document);
        ParserStrategy strategy = new ParserStrategy(){

            @Override
            public void initParams() {
                resultKey = "bios";
                Bios bios = new Bios();
                firstRequiredTagName = "BIOS";
                requiredTagsNames = Utils.getFieldsNames(bios);
                for( String tag : requiredTagsNames ){
                    foundTags.put(tag, false);
                }
                result.put(resultKey, bios);
            }
            
            @Override
            public Map<String, Object> parsingAlgorithm(XMLStreamReader parser, Map<String, Object> resultMap)
                    throws XMLStreamException {
                
                return defaultParsingAlgorithm(parser, resultMap);                        
            }           
   
        };
                
        Map<String,Object> resultMap = strategy.parseContent(parser);
        return (Bios) resultMap.get(strategy.getResultKey());
        
    }
    
    public Hardware parseHardware(String document){
        XMLStreamReader parser = XmlParser.createParser(document);
        ParserStrategy strategy = new ParserStrategy(){

            @Override
            public void initParams() {
                resultKey = "hardware";
                Hardware hardware = new Hardware();
                firstRequiredTagName = "HARDWARE";
                requiredTagsNames = Utils.getFieldsNames(hardware);
                for( String tag : requiredTagsNames ){
                    foundTags.put(tag, false);
                }
                result.put(resultKey, hardware);
            }
            
            @Override
            public Map<String, Object> parsingAlgorithm(XMLStreamReader parser, Map<String, Object> resultMap) throws XMLStreamException {
                return defaultParsingAlgorithm(parser, resultMap);
            }                 
                        
        };
        Map<String,Object> resultMap = strategy.parseContent(parser);
        return (Hardware) resultMap.get(strategy.getResultKey());        
                
    }
    
    
    public List<Software> parseSoftwares(String document){
           
        XMLStreamReader parser = XmlParser.createParser(document);
        ParserStrategy strategy = new ParserStrategy(){

            @Override
            public void initParams() {
                resultKey = "softwares";
                List<Software> softList = new ArrayList<Software>();
                firstRequiredTagName = "SOFTWARES";
                requiredTagsNames = Utils.getFieldsNames(new Software());
                for( String tag : requiredTagsNames ){
                    foundTags.put(tag, false);
                }
                result.put(resultKey, softList);
            }
            
            @Override
            public Map<String, Object> parsingAlgorithm(XMLStreamReader parser, Map<String, Object> resultMap) throws XMLStreamException {
                return parsingListAlgorithm(parser, resultMap, Software.class);
            }

        };
        Map<String,Object> resultMap = strategy.parseContent(parser);
        return ( List<Software> ) resultMap.get(strategy.getResultKey());
        
    }
}
