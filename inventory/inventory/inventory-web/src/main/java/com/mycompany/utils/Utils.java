/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.utils;

import com.mycompany.domain.Bios;
import com.mycompany.domain.DomainObject;
import com.mycompany.domain.Hardware;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 *
 * @author rudolph
 */
public class Utils {
    
    public static final String UTF8_BOM = "\uFEFF";
    
    public static final String ENCODING = "ISO-8859-1";
    
    public static String unzip(byte[] bytes){
                
        StringBuilder builder = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(
                                        new InputStreamReader(                            
                                                new GZIPInputStream(
                                                new ByteArrayInputStream(bytes))));
            boolean firstLine = true;
            
            String str;
            while((str = reader.readLine()) != null ){
                // discard utf8 byte order mask
                /*
                if(firstLine){
                    str = Utils.removeUtf8BOM(str) + System.getProperty("line.separator");
                    firstLine = false;
                }
                */
                builder.append(str.trim());
                builder.append(System.getProperty("line.separator"));
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
        return builder.toString();
    }
    
    public static String zip(String unzippedContent){
        
        
            
        if (unzippedContent == null || unzippedContent.length() == 0) {
            return unzippedContent;
        }

        byte[] bytes = unzippedContent.getBytes();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip;
        try {
            gzip = new GZIPOutputStream(out);
            gzip.write(bytes);
            
            //gzip.finish();
            gzip.close();
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }

        String res = null;
        try {
            res = new String(out.toByteArray(),"UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return res;
            
        
                
    }
    
    public static String compress(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        System.out.println("String length : " + str.length());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(out);
        gzip.write(str.getBytes());
        gzip.close();
        String outStr = out.toString(ENCODING);
        System.out.println("Output String lenght : " + outStr.length());
        //System.out.println("Output String lenght : " + outStr.length());
        return outStr;
     }
    
    public static String decompress(String str) throws IOException {
        if (str == null || str.length() == 0) {
            return str;
        }
        System.out.println("Input String length : " + str.length());
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(str.getBytes(ENCODING)));
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, ENCODING));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
          outStr += line;
        }
        System.out.println("Output String lenght : " + outStr.length());
        return outStr;
     }
    
    public static String decompressFromStream(InputStream stream) throws IOException{
    
        GZIPInputStream gis = new GZIPInputStream(stream);
        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, ENCODING));
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
          outStr += line;
        }
                
        return outStr;
    }
    
    
    /*
    public static String readFromFile(String path){
        System.out.println("readFromFile");
        StringBuilder builder = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(path)));
            String str;
            while((str = reader.readLine()) != null ){
                                
                builder.append(str);                
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();                                        
    }
    
    public static void writeToFile(String path,byte[] bytes){
        System.out.println("writeToFile");
        try {
            BufferedReader reader = new BufferedReader(
                                        new InputStreamReader(                            
                                                new GZIPInputStream(
                                                new ByteArrayInputStream(bytes))));
                        
            boolean firstLine = true;
            FileOutputStream fos = new FileOutputStream(new File(path));
            Writer w = new BufferedWriter(new OutputStreamWriter(fos, "UTF-8"));
            
            
            String str;
            while((str = reader.readLine()) != null ){
                // discard utf8 byte order mask
                if(firstLine){
                    str = Utils.removeUtf8BOM(str);// + System.getProperty("line.separator");
                    firstLine = false;
                    
                }
                w.write(str + System.getProperty("line.separator"));
                w.flush();
                
                //builder.append(str);
            }
            fos.close();
            
        } catch (IOException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                //return null;
       }
       
    }
    */
    
    /*
    public static String removeUtf8BOM(String s){
        System.out.println("removeUtf8BOM!");
        String res;
        
        //return "<?xml version =\"1.0\" encoding=\"utf-8\" ?>";
        return s;
    }
    */
    
    
    public static List<String> getFieldsNames(DomainObject domainObj){
        
        List<Field> fields;
        List<String> fieldNames = new ArrayList<>();        
                    
        fields = new ArrayList<>(Arrays.asList(
                                   domainObj.getClass().getDeclaredFields()));

        for( Field field : fields ){
            fieldNames.add(field.getName());
        }
        
        return fieldNames;      
       
    }
    
    public static String firstUpperCase(String word){
	if(word == null || word.isEmpty()) return "";//или return word;
	return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
    
    public static String getSetterMethodName(String fieldName){
        StringBuilder builder = new StringBuilder();
        return builder.append("set").append(firstUpperCase(fieldName)).toString();
    }
    
    public static <T> Object invokeMethodWithOneArg(DomainObject domainObj, String methodName,Class[] argType, T arg ){
           
        try {            
            Method m = domainObj.getClass().getDeclaredMethod(methodName, argType);
            try {
                return m.invoke(domainObj, arg);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
    
    /*
    public static void main(String[] args) throws NoSuchFieldException{
        try {
            String s = "osversionddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
            //System.out.println(Utils.getSetterMethodName(s));
            System.out.println("Before zip =" + s);
            //String str = Utils.zip(s);
            String str = Utils.compress(s);
            System.out.println("After zip =" + str);
            //String s2 = Utils.unzip(str.getBytes());
            String s2 = Utils.decompress(str);
            System.out.println("After unzip =" + s2);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    */
    
}
