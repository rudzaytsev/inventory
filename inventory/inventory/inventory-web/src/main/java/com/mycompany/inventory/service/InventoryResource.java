/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.inventory.service;

import com.mycompany.domain.Device;
import com.mycompany.utils.Utils;
import com.mycompany.utils.XmlGenerator;
import com.mycompany.utils.XmlParser;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.http.entity.StringEntity;

/**
 * REST Web Service
 *
 * @author rudolph
 */
@Path("service")
@RequestScoped
public class InventoryResource {
    
    private static final String UNKNOWN_REQUEST = "unknown request";
    private static final String PROLOG_REQUEST = "PROLOG";
    private static final String INVENTORY_REQUEST = "INVENTORY";
    
    @Context
    private UriInfo context;
    
    @Resource(mappedName="jms/DeviceMessageFactory")
    private  ConnectionFactory connectionFactory;

    @Resource(mappedName="jms/DeviceMessage")
    private  Queue queue;

    /**
     * Creates a new instance of InventoryResource
     */
    public InventoryResource() {
    }

   
    
    @POST
    @Consumes("text/plain")    
    @Produces("text/plain")
    public Response inventory(String content) {
       System.out.println("*****************************************************************************"); 
       System.out.println("Connected to web service");
       System.out.println(content);
       String responseContent = this.getResponseContent(content);
       if(!responseContent.equals(UNKNOWN_REQUEST) ){
          StringEntity entity = new StringEntity(responseContent, Utils.ENCODING);
          return Response.status(200).entity(entity).type(MediaType.TEXT_PLAIN).build();
          
       }
       else {
           return Response.serverError().entity(responseContent).build();
       }
       
       //System.out.println("*****************************************************************************");
    }
    
    public void sendDeviceInventoryMessage(Device device){
                
            try (Connection connection = connectionFactory.createConnection()) {
                Session session = connection.createSession(false,Session.AUTO_ACKNOWLEDGE );
                try (MessageProducer producer = session.createProducer(queue)) {
                    ObjectMessage msg = session.createObjectMessage(device);
                    producer.send(msg);
                }
                
            }           
            catch (JMSException ex) {
                Logger.getLogger(InventoryResource.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
     
    public String getResponseContent(String document){
        String command = parseXMLDocument(document);
        return generateResponseContent(command);
    }
    
    public String parseXMLDocument(String document){
        XmlParser parser = new XmlParser();
        try {
            Device device = parser.parseDocument(document);
            
            if(device == null) return PROLOG_REQUEST;
            else{
                System.out.println(device);
                this.sendDeviceInventoryMessage(device);
                return INVENTORY_REQUEST;
            }
            
        } catch (Exception ex) {
           // Logger.getLogger(InventoryServer.class.getName()).log(Level.SEVERE, null, ex);
            return UNKNOWN_REQUEST;
        }
        
    }
    
    public String generateResponseContent(String command){
        //TODO implement set diferent content depends on command = [PROLOG | INVENTORY | UNKNOWN]
        XmlGenerator generator = new XmlGenerator();
        
        switch(command){
            case PROLOG_REQUEST:    return generator.getPrologResponse();    
            case INVENTORY_REQUEST: return generator.getInventoryResponse(); 
            default: return UNKNOWN_REQUEST;/* UNKNOWN Error case  */    
        }
        
    }
}
