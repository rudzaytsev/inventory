/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.inventory.service;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import org.apache.http.entity.StringEntity;

/**
 *
 * @author rudolph
 */
@Provider
@Produces("text/plain")
public class InventoryWriter implements MessageBodyWriter<StringEntity> {

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
       
        return true;
    }

    @Override
    public long getSize(StringEntity t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
         return t.getContentLength();
    }

    @Override
    public void writeTo(StringEntity t, Class<?> type, Type genericType, Annotation[] annotations,
            MediaType mediaType, MultivaluedMap<String, Object> httpHeaders,
            OutputStream entityStream) throws IOException, WebApplicationException {
        
            t.writeTo(entityStream);
            System.out.println("called InventoryWriter");
    }
}
