/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.servlets;

import com.mycompany.DeviceBean;
import com.mycompany.domain.Device;
import static com.mycompany.servlets.LoginServlet.SUCCESS_AUTH_REDIRECT_URI;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rudolph
 */
public class InventoryServlet extends HttpServlet {
    
    @EJB            
    DeviceBean deviceBean;
    
    public static final String SHOW_DEVICE_INFO_URI = "device_info.jsp";
    public static final String UPDATE_INVENTORY_URI = "inventory.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession(false);
        if(session == null){
            session = request.getSession(true);
        }
        
        String updateParam = request.getParameter("inventoryupdate");
        
        if((updateParam != null) &&  "UPDATE".equals(updateParam)){
               System.out.println("**** UPDATE IS NOT NULL **********************");
               List<Device> devices = deviceBean.findAllDevices();
               session.setAttribute(ServletsUtils.DEVICES,devices);
               session.setAttribute(ServletsUtils.AUTH_KEY,ServletsUtils.AUTH_VALUE);
               response.sendRedirect(UPDATE_INVENTORY_URI);
        }
        else{
            System.out.println("**** UPDATE IS NULL **********************");
            String deviceIdStr = request.getParameter("id");
            Long id = Long.parseLong(deviceIdStr);

            session.setAttribute(ServletsUtils.AUTH_KEY,ServletsUtils.AUTH_VALUE);
            Device device = deviceBean.findDeviceById(id);
            if(device != null){
                session.setAttribute(ServletsUtils.WATCHING_DEVICE,device);
                response.sendRedirect(SHOW_DEVICE_INFO_URI);
            }
            
        
            try (PrintWriter out = response.getWriter()) {

                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet InventoryServlet</title>");            
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Servlet InventoryServlet Error Watching Device is NULL </h1>");
                out.println("</body>");
                out.println("</html>");
            }
        }
        
           
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
