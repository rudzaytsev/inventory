/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.utils;


import com.mycompany.domain.DomainObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

/**
 *
 * @author rudolph
 */
public abstract class ParserStrategy {
    
    protected String resultKey = "key";
    protected String firstRequiredTagName = "";
    protected boolean firstRequiredTagArrived = false;
    
    protected List<String> requiredTagsNames = new ArrayList<String>();
    protected Map<String,Boolean> foundTags = new HashMap<>();
    protected Map<String,Object> result = new HashMap<>();
    
    
    
    public Map<String, Object> parseContent(XMLStreamReader parser){
        //Map<String,Object> result = new HashMap<>();
        initParams();        
        try {
            while(parser.hasNext()){
                
                parsingAlgorithm(parser,result);                
            }
        } catch (XMLStreamException ex) {
            Logger.getLogger(ParserStrategy.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public abstract Map<String, Object> parsingAlgorithm(XMLStreamReader parser, Map<String,Object> resultMap)
                                        throws XMLStreamException;

    public abstract void initParams();
    
    protected Map<String, Object> defaultParsingAlgorithm(XMLStreamReader parser, Map<String,Object> resultMap)
                                        throws XMLStreamException{
        
            int event = parser.next();
            if(event == XMLStreamConstants.START_ELEMENT){
                String elementName = parser.getLocalName();
                if(elementName.equals(firstRequiredTagName)){
                    firstRequiredTagArrived = true;
                }
                if(firstRequiredTagArrived){
                    for(String tagName : requiredTagsNames ){
                       if(elementName.equalsIgnoreCase(tagName)){
                           foundTags.put(tagName, true);
                           break;
                       }
                    }
                }
            }
            else if(event == XMLStreamConstants.CHARACTERS){
                if(firstRequiredTagArrived){
                    for(String tag  : foundTags.keySet()){
                        boolean found = foundTags.get(tag);
                        if(found){
                            // get method to set tag value to domain object
                            DomainObject obj = (DomainObject) result.get(resultKey);
                            String data = parser.getText();
                            Utils.invokeMethodWithOneArg(
                                    obj,Utils.getSetterMethodName(tag),new Class[]{String.class}, data);
                            foundTags.put(tag, false);
                        }
                    }
                }    
            }
            else if(event == XMLStreamConstants.END_ELEMENT){
                if(firstRequiredTagArrived){
                    String elementName = parser.getLocalName();
                    if(elementName.equals(firstRequiredTagName)){
                        firstRequiredTagArrived = false;
                    }
                }
            }

            return result;    
    }
    
    protected Map<String, Object> parsingListAlgorithm(XMLStreamReader parser, Map<String,Object> resultMap,
                                                       Class< ? extends DomainObject> listElementType) throws XMLStreamException{
            int event = parser.next();
            if(event == XMLStreamConstants.START_ELEMENT){
                String elementName = parser.getLocalName();
                if(elementName.equals(firstRequiredTagName)){
                    firstRequiredTagArrived = true;
                    List<DomainObject> domainObjects = (List<DomainObject>) result.get(resultKey);
                    try {
                        domainObjects.add(listElementType.newInstance());
                    } catch (InstantiationException | IllegalAccessException ex) {
                        Logger.getLogger(ParserStrategy.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(firstRequiredTagArrived){
                    for(String tagName : requiredTagsNames ){
                       if(elementName.equalsIgnoreCase(tagName)){
                           foundTags.put(tagName, true);
                           break;
                       }
                    }
                }
            }
            else if(event == XMLStreamConstants.CHARACTERS){
                if(firstRequiredTagArrived){
                    for(String tag  : foundTags.keySet()){
                        boolean found = foundTags.get(tag);
                        if(found){
                            // get method to set tag value to 
                            List<DomainObject> listObjs = (List<DomainObject>) result.get(resultKey);
                            String data = parser.getText();
                            int lastElemIndex = listObjs.size() - 1;
                            if (lastElemIndex >= 0 ){
                                Utils.invokeMethodWithOneArg(
                                        listObjs.get(lastElemIndex),
                                        Utils.getSetterMethodName(tag),
                                        new Class[]{String.class},
                                        data);                            
                            }
                            foundTags.put(tag, false);
                        }
                    }
                }    
            }
            else if(event == XMLStreamConstants.END_ELEMENT){
                if(firstRequiredTagArrived){
                    String elementName = parser.getLocalName();
                    if(elementName.equals(firstRequiredTagName)){
                        firstRequiredTagArrived = false;
                    }
                }
            }
            return result;
    }
    
    public String getRequiredTagName() {
        return firstRequiredTagName;
    }

    public void setRequiredTagName(String requiredTagName) {
        this.firstRequiredTagName = requiredTagName;
    }

    public boolean isFirstRequiredTagArrived() {
        return firstRequiredTagArrived;
    }

    public void setFirstRequiredTagArrived(boolean firstRequiredTagArrived) {
        this.firstRequiredTagArrived = firstRequiredTagArrived;
    }

    public List<String> getRequiredTagsNames() {
        return requiredTagsNames;
    }

    public void setRequiredTagsNames(List<String> requiredTagsNames) {
        this.requiredTagsNames = requiredTagsNames;
    }
    
    public ParserStrategy addRequiredTag(String tagName){
        if(!requiredTagsNames.contains(tagName)) {
            requiredTagsNames.add(tagName);
        }
        return this;
    }

    public String getResultKey() {
        return resultKey;
    }

    public void setResultKey(String resultKey) {
        this.resultKey = resultKey;
    }
    
    
    
}
