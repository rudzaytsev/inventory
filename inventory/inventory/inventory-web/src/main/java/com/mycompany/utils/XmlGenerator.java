/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;


/**
 *
 * @author rudolph
 */
public class XmlGenerator {
    
    
   /**
    * Response example:
    *
    * <PARAM ID="1365231890"
    *  SCHEDULE="" CERT_FILE="INSTALL_PATH/cacert.pem" TYPE="PACK" INFO_LOC="localhost/download"
    *  CERT_PATH="INSTALL_PATH" PACK_LOC="localhost/download" FORCE="0" POSTCMD="" />
    *
    */    
    public String generatePrologResponse(boolean success){
        XMLStreamWriter writer = null;
        ByteArrayOutputStream bytesIn = new ByteArrayOutputStream();
        try {
           
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
           
           
            writer = factory.createXMLStreamWriter(bytesIn, "UTF8"); 
            
            //writer.writeStartDocument("UTF-8", "1.0");
            writer.writeStartDocument();
            writer.writeStartElement("REPLY");
            writer.writeStartElement("RESPONSE");
            if(success){
                writer.writeCharacters("SUCCESS");
            }
            else {
                writer.writeCharacters("ERROR");
            }
            writer.writeEndElement();
            
            
            /*
            writer.writeStartElement("PARAM");
            writer.writeAttribute("ID","1365231890");
            writer.writeAttribute("SCHEDULE","");
            writer.writeAttribute("CERT_FILE","INSTALL_PATH/cacert.pem");
            writer.writeAttribute("TYPE", "PACK");
            writer.writeAttribute("INFO_LOC", "localhost/download");
            writer.writeAttribute("CERT_PATH","INSTALL_PATH");
            writer.writeAttribute("PACK_LOC", "localhost/download");
            writer.writeAttribute("FORCE", "0");
            writer.writeAttribute("POSTCMD", "");                   
            writer.writeEndElement();
            */
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.close();
            System.out.println("res = " + bytesIn.toString("UTF8"));
            
            return bytesIn.toString("UTF8");           
            
        } catch (XMLStreamException ex) {
            Logger.getLogger(XmlGenerator.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            System.out.println("exception = " + ex.getMessage());
        }
        catch (UnsupportedEncodingException ex) {
                Logger.getLogger(XmlGenerator.class.getName()).log(Level.SEVERE, null, ex);
                ex.printStackTrace();
                System.out.println("exception = " + ex.getMessage());
                return "";
        }
            
        return "";
        
    }
    
    public String getPrologResponse(){
        
        String content = this.generatePrologResponse(true);
        String gzippedContent = null;
        try {
            gzippedContent =  Utils.compress(content);
        } catch (IOException ex) {
            Logger.getLogger(XmlGenerator.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("IO Exception after COMPRESS = " + ex.getMessage());
            gzippedContent = content;
        }
        System.out.println("gzippedContent = = " + gzippedContent);
        
        return gzippedContent;
    }
    
    public String getInventoryResponse(){
        
        String content = this.generateInventoryResponse(true);
        String gzippedContent = null;
        try {
            gzippedContent =  Utils.compress(content);
        } catch (IOException ex) {
            Logger.getLogger(XmlGenerator.class.getName()).log(Level.SEVERE, null, ex);
            gzippedContent = content;
        }
            
        return gzippedContent;
    }
    
    public String generateInventoryResponse(boolean success){
        
        try {
            XMLOutputFactory factory = XMLOutputFactory.newInstance();
            ByteArrayOutputStream bytesIn = new ByteArrayOutputStream();
            
            XMLStreamWriter writer = factory.createXMLStreamWriter(bytesIn, "UTF8");
            
            writer.writeStartDocument();
            writer.writeStartElement("REPLY");
            writer.writeStartElement("RESPONSE");
            if(success){
                writer.writeCharacters("Success");
            }
            else {
                writer.writeCharacters("Error");
            }
            writer.writeEndElement();
            writer.writeEndElement();
            
            writer.writeEndDocument();
            
            writer.close();
            
            return bytesIn.toString("UTF8");
            
        } catch (XMLStreamException ex) {
            Logger.getLogger(XmlGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (UnsupportedEncodingException ex) {
            Logger.getLogger(XmlGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
}
