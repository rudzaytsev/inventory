/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.utils.Utils;
import com.mycompany.utils.XmlGenerator;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rudolph
 */
public class CompressDecompressTest {
    
    public CompressDecompressTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCompress() throws IOException {
    
       XmlGenerator g = new XmlGenerator();
       String expectedRes = g.generatePrologResponse(true);
       String res = Utils.decompress(g.getPrologResponse());
       
        assertEquals(expectedRes, res);
        System.out.println("expRes" + expectedRes);
        System.out.println("factRes" + res);
    }
}
