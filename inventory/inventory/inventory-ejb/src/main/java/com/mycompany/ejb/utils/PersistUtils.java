/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.ejb.utils;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author rudolph
 */
public class PersistUtils {
    
    private static final String persistUnit = "com.mycompany_inventory-ejb_ejb_1.0-SNAPSHOTPU";
    
    public static EntityManager getEntityManager(){
        return Persistence.createEntityManagerFactory(persistUnit).createEntityManager();
    }
    
}
