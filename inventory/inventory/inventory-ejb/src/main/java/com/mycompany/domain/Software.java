/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.domain;

import javax.persistence.Entity;

/**
 *
 * @author rudolph
 */
@Entity
public class Software extends DomainObject {
    
    
    private String publisher = "";
    private String name = "";
    private String folder = "";
    private String version = "";
    private String installDate  = "";
    private String comments = "";
    private String fileSize = "";

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getInstallDate() {
        return installDate;
    }

    public void setInstallDate(String installDate) {
        this.installDate = installDate;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public String toString() {
        return "Software{" + "publisher=" + publisher + ", name=" + name + ", folder=" + folder + ", version=" + version + ", installDate=" + installDate + ", comments=" + comments + ", fileSize=" + fileSize + '}';
    }
    
    
    
    
}
