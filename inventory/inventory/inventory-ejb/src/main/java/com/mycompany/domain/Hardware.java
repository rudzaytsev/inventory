/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.domain;

import javax.persistence.Entity;

/**
 *
 * @author rudolph
 */
@Entity
public class Hardware extends DomainObject {
   
    private String processorT = "";
    private String lastLoggedUser = "";
    private String processorS = "";
    private String name = "";
    private String osname = "";
    private String swap = "";
    private String workgroup = "";
    private String ipAddr = "";
    private String dateLastLoggedUser = "";
    private String osversion = "";
    private String memory = "";
    private String oscomments = "";
    private String processorN = "";
    private String checkSum = "";
    private String userId = "";

    public String getProcessorT() {
        return processorT;
    }

    public void setProcessorT(String processorT) {
        this.processorT = processorT;
    }

    public String getLastLoggedUser() {
        return lastLoggedUser;
    }

    public void setLastLoggedUser(String lastLoggedUser) {
        this.lastLoggedUser = lastLoggedUser;
    }

    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOsname() {
        return osname;
    }

    public void setOsname(String osname) {
        this.osname = osname;
    }

    

    public String getWorkgroup() {
        return workgroup;
    }

    public void setWorkgroup(String workgroup) {
        this.workgroup = workgroup;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public String getDateLastLoggedUser() {
        return dateLastLoggedUser;
    }

    public void setDateLastLoggedUser(String dateLastLoggedUser) {
        this.dateLastLoggedUser = dateLastLoggedUser;
    }

    public String getOsversion() {
        return osversion;
    }

    public void setOsversion(String osversion) {
        this.osversion = osversion;
    }

    public String getOscomments() {
        return oscomments;
    }

    public void setOscomments(String oscomments) {
        this.oscomments = oscomments;
    }
    
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProcessorS() {
        return processorS;
    }

    public void setProcessorS(String processorS) {
        this.processorS = processorS;
    }

    public String getSwap() {
        return swap;
    }

    public void setSwap(String swap) {
        this.swap = swap;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public String getProcessorN() {
        return processorN;
    }

    public void setProcessorN(String processorN) {
        this.processorN = processorN;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    @Override
    public String toString() {
        return "Hardware{" + "processorT=" + processorT + ", lastLoggedUser=" + lastLoggedUser + ","
                + " processorS=" + processorS + ", name=" + name + ", osname=" + osname + ", swap=" + swap + ","
                + " workgroup=" + workgroup + ", ipAddr=" + ipAddr + ", dateLastLoggedUser=" + dateLastLoggedUser
                + ", osversion=" + osversion + ", memory=" + memory + ", oscomments=" + oscomments +
                ", processorN=" + processorN + ", checkSum=" + checkSum + ", userId=" + userId + '}';
    }

    
      
}
