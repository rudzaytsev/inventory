/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author rudolph
 */
@Entity
@NamedQueries({
  @NamedQuery(name = "Device.findAll", query = "select d from Device d"),
  @NamedQuery(name = "Device.findById",query = "select d from Device d where d.id = :id")  
})
public class Device extends DomainObject {
    
    private String osName;
    private String deviceId;
    
    /**
     * Timestamp of last device inventory
     * as String in format: YYYY-MM-DD-hh-mm-ss 
     * 
     */
    private String deviceTimeStamp;
    
    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST )
    @JoinColumn(name = "bios_fk", nullable = true)
    private Bios bios = new Bios();
    
    @OneToOne(fetch = FetchType.EAGER,cascade = CascadeType.PERSIST )
    @JoinColumn(name = "hardware_fk", nullable = true)
    private Hardware hardware = new Hardware();
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "software_fk", nullable = true)
    private List<Software> softwares = new ArrayList<Software>();
    
    /**
     * 
     * @param deviceIdFromAgent - deviceId String from mobile agent
     * Example: android-445f0a4888f42da8-2014-09-07-16-12-16
     */    
    public Device(String deviceIdFromAgent){
        /*
        String[] parts =  deviceIdFromAgent.split("-",3);
        for(int i = 0; i < parts.length; i++){
            if(i == 0 ){
                osName = parts[0];
            }
            else if(i == 1){
                deviceId = parts[1];
            }
            else if(i == 2) {
                deviceTimeStamp = parts[2];
            }
        }
        */
        deviceId = deviceIdFromAgent;
    }

    public Device() {        
    }

    public String getOsName() {
        return osName;
    }

    public void setOsName(String osName) {
        this.osName = osName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceTimeStamp() {
        return deviceTimeStamp;
    }

    public void setDeviceTimeStamp(String deviceTimeStamp) {
        this.deviceTimeStamp = deviceTimeStamp;
    }

    public Bios getBios() {
        return bios;
    }

    public void setBios(Bios bios) {
        this.bios = bios;
    }

    public Hardware getHardware() {
        return hardware;
    }

    public void setHardware(Hardware hardware) {
        this.hardware = hardware;
    }

    public List<Software> getSoftwares() {
        return softwares;
    }

    public void setSoftwares(List<Software> softwares) {
        this.softwares = softwares;
    }
    
    public void addSoftware(Software soft){
        softwares.add(soft);
    }

    @Override
    public String toString() {
        return "Device{" + "osName=" + osName + ", deviceId=" + deviceId + ", timeStamp=" + deviceTimeStamp +
                ", bios=" + bios + ", hardware=" + hardware + ", softwares=" + softwares + '}';
    }
    
    
}
