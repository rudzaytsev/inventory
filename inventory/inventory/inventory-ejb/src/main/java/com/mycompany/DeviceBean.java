/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany;

import com.mycompany.domain.Device;
import com.mycompany.ejb.utils.PersistUtils;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author martin
 */
@Stateless
@LocalBean
public class DeviceBean {

   
    
    public List<Device> findAllDevices(){
          EntityManager manager = PersistUtils.getEntityManager();
          TypedQuery<Device> query = manager.createNamedQuery("Device.findAll", Device.class);
          return query.getResultList();          
    }
    
    public Device findDeviceById(Long id){
         EntityManager manager = PersistUtils.getEntityManager();
         TypedQuery<Device> query = manager.createNamedQuery("Device.findById", Device.class);
         query.setParameter("id", id);
         return query.getSingleResult();
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

}
