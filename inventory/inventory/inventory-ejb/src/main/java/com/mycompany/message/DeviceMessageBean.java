/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.message;

import com.mycompany.domain.Device;
import com.mycompany.ejb.utils.PersistUtils;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 *
 * @author rudolph
 */
@JMSDestinationDefinition(name = "jms/DeviceMessage", interfaceName = "javax.jms.Queue",
                          resourceAdapter = "jmsra", destinationName = "DeviceMessage")
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/DeviceMessage"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class DeviceMessageBean implements MessageListener {
    
    //@PersistenceContext(name = "com.mycompany_inventory-ejb_ejb_1.0-SNAPSHOTPU")
    EntityManager entityManager;
        
    public DeviceMessageBean() {
    }
    
    @Override
    public void onMessage(Message message) {
                
        System.out.println("onMessage() called ***************************");
        ObjectMessage msg = null;
        if(message instanceof ObjectMessage){
            
            msg = (ObjectMessage) message;
            try {
                Device device = (Device) msg.getObject();
                System.out.println(device);
                this.store(device);
            } catch (JMSException ex) {
                Logger.getLogger(DeviceMessageBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    }
    
    private void store(Device device){
        
        entityManager = PersistUtils.getEntityManager();
                
        EntityTransaction tx = entityManager.getTransaction();
        
        tx.begin();        
        entityManager.persist(device);        
        tx.commit();
        entityManager.close();
    }
    
}
