/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.domain;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.Entity;

/**
 * This class represents information about 
 * BIOS of Device
 * 
 * @author rudolph
 */
@Entity
public class Bios extends DomainObject {
    
    private String assettag = "";
    private String smodel = "";
    private String ssn = "";
    private String bmanufacturer = "";
    private String smanufacturer = "";
    private String msn = "";
    private String bdate = "";
    private String mmanufacturer = "";
    private String bversion = "";
    private String type = "";

    public String getAssettag() {
        return assettag;
    }

    public void setAssettag(String assettag) {
        this.assettag = assettag;
    }

    public String getSmodel() {
        return smodel;
    }

    public void setSmodel(String smodel) {
        this.smodel = smodel;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getBmanufacturer() {
        return bmanufacturer;
    }

    public void setBmanufacturer(String bmanufacturer) {
        this.bmanufacturer = bmanufacturer;
    }

    public String getSmanufacturer() {
        return smanufacturer;
    }

    public void setSmanufacturer(String smanufacturer) {
        this.smanufacturer = smanufacturer;
    }

    public String getMsn() {
        return msn;
    }

    public void setMsn(String msn) {
        this.msn = msn;
    }

    public String getBdate() {
        return bdate;
    }

    public void setBdate(String bdate) {
        this.bdate = bdate;
    }

    public String getMmanufacturer() {
        return mmanufacturer;
    }

    public void setMmanufacturer(String mmanufacturer) {
        this.mmanufacturer = mmanufacturer;
    }

    public String getBversion() {
        return bversion;
    }

    public void setBversion(String bversion) {
        this.bversion = bversion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Bios{" + "assettag=" + assettag + ", smodel=" + smodel + ","
                + " ssn=" + ssn + ", bmanufacturer=" + bmanufacturer + ","
                + " smanufacturer=" + smanufacturer + ", msn=" + msn + ","
                + " bdate=" + bdate + ", mmanufacturer=" + mmanufacturer + ","
                + " bversion=" + bversion + ", type=" + type + '}';
    }

   
    
    
            
}
